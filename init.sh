#!/usr/bin/env bash

set -e

# unpack the template archive in a temporary directory
mkdir -p tmp
cd tmp
curl https://gitlab.com/empathy/vyper-dev-template/-/archive/master/vyper-dev-template-master.tar.gz -o vyper-dev-template-master.tar.gz
tar -x -f vyper-dev-template-master.tar.gz
cd ..

# only initialise example contracts and tests if they don't yet exist
if [ ! -f code/contracts/* ];
then
  mkdir -p code
  cp -R tmp/vyper-dev-template-master/template/code/contracts code/.
  cp -R tmp/vyper-dev-template-master/template/code/tests code/.
fi
rm -r tmp/vyper-dev-template-master/template/code/contracts
rm -r tmp/vyper-dev-template-master/template/code/tests

# initialise the rest of the template, overwriting existing files
cp -R tmp/vyper-dev-template-master/template/* .

# it's surprisingly difficult to glob all files starting with a dot in a
# shell-portable way, so we are making use of the fact that we are only copying
# Git and GitLab config files
cp -R tmp/vyper-dev-template-master/template/.git* .

# clean up the temporary directory
rm -r tmp
