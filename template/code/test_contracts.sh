#!/usr/bin/env bash

# exit the script on any simple command returning a nonzero exit value
set -e

cd tests

find . -name "test_*.py" | while read test
do
  echo "Running ${test}..."
  python3 $test
done

echo "All tests passed succesfully!"
