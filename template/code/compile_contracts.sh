#!/usr/bin/env bash

# exit the script on any simple command returning a nonzero exit value
set -e

# make a target directory (needed for the following delete commands)
mkdir -p target

# delete existing *.abi and *.bin files
find target -name *.abi -delete
find target -name *.bin -delete

# delete empty folders
find target -type d -empty -delete

# compile all Vyper contracts
find contracts -name *.vy | while read contract
do
  echo "Compiling ${contract}..."

  EXTENSIONLESS_PATH=${contract%.vy}
  NEW_EXTENSIONLESS_PATH=${EXTENSIONLESS_PATH/contracts/target}
  NEW_DIR=${NEW_EXTENSIONLESS_PATH%/*}

  mkdir -p $NEW_DIR
  vyper -f abi $contract > $NEW_EXTENSIONLESS_PATH.abi
  vyper $contract > $NEW_EXTENSIONLESS_PATH.bin
done
