greeting: bytes[20]

@public
def __init__():
    self.greeting = "Hello"

@public
def setGreeting(_greeting: bytes[20]):
    self.greeting = _greeting

@public
def greet() -> bytes[20]:
    return self.greeting
