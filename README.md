# Vyper dev template

## What is it?

Template of a Vyper project with a basic development environment.

The environment includes:

1. Vyper runtime based on the https://gitlab.com/empathy/vyper-dev-docker image.
2. Scripts for contract compilation and automated testing.
3. Git ignore file.
4. GitLab syntax highlighting for Vyper.
5. GitLab Continuous Integration config (building and testing your contracts).

## Requirements

1. Docker (installation instructions at https://docs.docker.com/install/).
2. Docker Compose (if you're on Linux, best installed using `pip install docker-compose` or `pip3 install docker-compose`).

## Usage

To initialise, run `curl https://gitlab.com/empathy/vyper-dev-template/raw/master/init.sh -sSf | sh` inside your project directory.

Note that if you already have some code, the initialisation script will not initialise the example contract and test.

To compile and test your contracts, run `./build.sh` and `./run_tests.sh` respectively.

## Philosophy

1. Lightweight -- Ethereum's Python and Vyper stack is still fragile, so only the bare minimum tools are installed.
2. Simple -- testing is done via simple Web3.py scripts with `assert` statement, akin to https://github.com/ethereum/web3.py#quickstart .
3. Bleeding edge -- Vyper is still in development, so the runtime Docker image installs it from GitHub master branch.
